---
title: About
permalink: /about/
---

<p>FSMK Camps is a platform for budding engineers and other enthusiasts to gather and learn about Free Software Technologies. It is an effort by the organization that helps to bridge students from concepts to applications via Free Software technologies.</p>

<p>FSMK has been actively organizing workshops in various engineering colleges across the state since last 5 years. However, we noticed that a longer duration residential camp completely dedicated to Free Software technologies will be of much greater help to students as they will be able to interact more with the speakers and get lot of time to practice their skills. Hence in 2013, FSMK conducted 2 residential camps for students which saw huge participation. In Jan 2013 we conducted a 5 days residential camp at SVIT, Rajankunte which was attended by around 160 students. Later in July 2013, we conducted a 9 days residential camp at JVIT, Bidadi which was attended by around 180 students and in 2014 same was conducted at DSCE , attended by 200 students. Our camps are organized by volunteers from the students community and also industry professionals. Similarly our resource persons are industry professionals from companies like Cisco, SourceBits, SAP Labs, Intel, MobiSir, Arista Networks, Intuit, etc.</p>

<p>As the organization is growing and with demand for more local workshops and camps, this year FSMK also conducted two regional camps at Mandya and Hassan, both of which were successful.</p>

<p>FSMK Camp'16 is, like previous camps, open to all districts within the state and anybody who is interested. We have planned amazing stuff for you all this year. Stay tuned.</p>
<p>Tracks for Camp'16 are:</p>
<ul>
<li>Web Technologies</li>
<li>Design and Animation</li>
<li>Freedom Hardware</li>
</ul>

<p>If you are looking to make this summer effective by learning and having fun, well, you are at the right place.</p>

To know more about Free Software Movement Karnataka (FSMK), check: <a href="http://fsmk.org" target="_blank">fsmk.org</a>.


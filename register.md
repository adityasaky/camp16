---
title: Register
permalink: /register/
---

<div class="full-view">
<div class="bot">
  <div class="message">
    Hi there! I'm Mooze.
  </div>

  <div class="cow">
    <pre>
    \   ^__^
     \  (oo)\_______
        (__)\       )\/\
            ||----w |
            ||     ||
    </pre>
  </div>
  <div class="textIn">
    <input type="text" class="textInput" name="message" placeholder="Say Moo or type /help">
  </div>
  <div class="buttons">
    <a href="#" class="shortcuts">/help</a>
    <a href="#" class="shortcuts">/about</a>
    <a href="#" class="shortcuts">/register</a>
  </div>
</div>
</div>

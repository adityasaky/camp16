---
title: Tracks
permalink: /tracks/
---

<p>We have three tracks for this year's FSMK Camp. They are:</p>

<div class="full-view">
<ul>
  <li><a href="#web">Web Technologies</a></li>
  <li><a href="#animation">Animation</a></li>
  <li><a href="#hardware">Hardware</a></li>
</ul>
</div>

<div class="track">
<a name="web"></a>
<h2 class="page-title" style="text-align: left;">Web Technologies<div class="page-title-stop">.</div></h2>
<p>
  Moar details coming soon(TM)!
</p>
<p>
  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ullamcorper justo id tempus laoreet. Nunc in sollicitudin sem. Curabitur vitae justo orci. Sed ut ligula nec justo dictum elementum vitae viverra ligula. Vivamus vel mi imperdiet, aliquet eros quis, hendrerit lorem. Fusce aliquet, turpis fringilla viverra porttitor, enim nunc suscipit neque, nec placerat eros tortor sit amet mi. Integer non elit auctor, faucibus elit sodales, rhoncus est. In sit amet consequat massa. In hac habitasse platea dictumst. Donec commodo fermentum faucibus. Ut vel quam hendrerit, lobortis mauris commodo, venenatis mauris.
</p>
<p>
Pellentesque sit amet nunc orci. Nam sit amet mi et sem hendrerit bibendum nec at ligula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Etiam ligula ligula, pellentesque vel justo et, semper tincidunt massa. Fusce auctor nulla leo, vitae feugiat augue vulputate quis. Etiam vitae justo erat. Quisque accumsan feugiat ex, nec pellentesque ligula lobortis fermentum. Sed tristique dolor ex, sed iaculis nibh placerat sed. Duis imperdiet, tellus non fermentum semper, augue est vehicula nisl, eget euismod ipsum ipsum ac libero. Curabitur dapibus, sem ut aliquet tempor, est est vestibulum dolor, sit amet aliquet mauris sapien vel tellus. Aliquam erat volutpat.
</p>
</div>

<div class="track">
<a name="animation"></a>
<h2 class="page-title" style="text-align: left;">Animation<div class="page-title-stop">.</div></h2>
<p>
  Moar details coming soon(TM)!
</p>
<p>
  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ullamcorper justo id tempus laoreet. Nunc in sollicitudin sem. Curabitur vitae justo orci. Sed ut ligula nec justo dictum elementum vitae viverra ligula. Vivamus vel mi imperdiet, aliquet eros quis, hendrerit lorem. Fusce aliquet, turpis fringilla viverra porttitor, enim nunc suscipit neque, nec placerat eros tortor sit amet mi. Integer non elit auctor, faucibus elit sodales, rhoncus est. In sit amet consequat massa. In hac habitasse platea dictumst. Donec commodo fermentum faucibus. Ut vel quam hendrerit, lobortis mauris commodo, venenatis mauris.
</p>
<p>
Pellentesque sit amet nunc orci. Nam sit amet mi et sem hendrerit bibendum nec at ligula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Etiam ligula ligula, pellentesque vel justo et, semper tincidunt massa. Fusce auctor nulla leo, vitae feugiat augue vulputate quis. Etiam vitae justo erat. Quisque accumsan feugiat ex, nec pellentesque ligula lobortis fermentum. Sed tristique dolor ex, sed iaculis nibh placerat sed. Duis imperdiet, tellus non fermentum semper, augue est vehicula nisl, eget euismod ipsum ipsum ac libero. Curabitur dapibus, sem ut aliquet tempor, est est vestibulum dolor, sit amet aliquet mauris sapien vel tellus. Aliquam erat volutpat.
</p>
</div>

<div class="track">
<a name="hardware"></a>
<h2 class="page-title" style="text-align: left;">Hardware<div class="page-title-stop">.</div></h2>
<p>
  Moar details coming soon(TM)!
</p>
<p>
  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ullamcorper justo id tempus laoreet. Nunc in sollicitudin sem. Curabitur vitae justo orci. Sed ut ligula nec justo dictum elementum vitae viverra ligula. Vivamus vel mi imperdiet, aliquet eros quis, hendrerit lorem. Fusce aliquet, turpis fringilla viverra porttitor, enim nunc suscipit neque, nec placerat eros tortor sit amet mi. Integer non elit auctor, faucibus elit sodales, rhoncus est. In sit amet consequat massa. In hac habitasse platea dictumst. Donec commodo fermentum faucibus. Ut vel quam hendrerit, lobortis mauris commodo, venenatis mauris.
</p>
<p>
Pellentesque sit amet nunc orci. Nam sit amet mi et sem hendrerit bibendum nec at ligula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Etiam ligula ligula, pellentesque vel justo et, semper tincidunt massa. Fusce auctor nulla leo, vitae feugiat augue vulputate quis. Etiam vitae justo erat. Quisque accumsan feugiat ex, nec pellentesque ligula lobortis fermentum. Sed tristique dolor ex, sed iaculis nibh placerat sed. Duis imperdiet, tellus non fermentum semper, augue est vehicula nisl, eget euismod ipsum ipsum ac libero. Curabitur dapibus, sem ut aliquet tempor, est est vestibulum dolor, sit amet aliquet mauris sapien vel tellus. Aliquam erat volutpat.
</p>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script type="text/javascript">
$(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});
</script>

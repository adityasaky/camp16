---
title: Frequently Asked Questions
permalink: /faqs/
---

<div class="faqs">
  <div class="question">
    What is FSMK?
  </div>
  <div class="answer">
    <a href="https://fsmk.org" target="_blank">Free Software Movement Karnataka (FSMK)</a> is a registered not-for-profit organization whose primary objective is to spread and create awareness about free software technologies amongst different sections of the society. FSMK is driven by volunteers from various backgrounds like software professionals, government officials, academicians and students.
  </div>
</div>
<hr>
<div class="faqs">
  <div class="question">
    What is FSMK Summer Camp?
  </div>
  <div class="answer">
    FSMK Summer Camp is an annual industry-oriented technical <a href="{{ site.baseurl }}/about">camp</a> hosted by Free Software Movement Karnataka to provide students a platdorm to learn Free Software technologies and tools. And oh, it's Free as in Freedom, not free biryani.
  </div>
</div>
<hr>
<div class="faqs">
  <div class="question">
    Who is the target audience?
  </div>
  <div class="answer">
    The workshop is conducted for the benefit of college students and Free Software / Technology enthusiasts.
  </div>
</div>
<hr>
<div class="faqs">
  <div class="question">
    I don't know what Free Software or GNU/Linux is. Is the camp for me?
  </div>
  <div class="answer">
    We have all been there. We start from the basics. You're most welcome!
  </div>
</div>
<hr>
<div class="faqs">
  <div class="question">
    What do participants learn?
  </div>
  <div class="answer">
    Participants are first introduced to Free Software technologies and tools and are then taught the technologies they have opted for while registering.
  </div>
</div>
<hr>
<div class="faqs">
  <div class="question">
    Will this be a classroom session?
  </div>
  <div class="answer">
    No, we will have a hands-on session where you can deploy what you learn on your system.
  </div>
</div>
<hr>
<div class="faqs">
  <div class="question">
    How much does this workshop cost?
  </div>
  <div class="answer">
    We'll let you know soon.
  </div>
</div>
<hr>
<div class="faqs">
  <div class="question">
    How do I register?
  </div>
  <div class="answer">
    Registrations haven't opened yet.
  </div>
</div>
<hr>
<div class="faqs">
  <div class="question">
    Should participants bring their laptops?
  </div>
  <div class="answer">
    Yes, it is preferable that participants carry their own laptops.
  </div>
</div>
<hr>
<div class="faqs">
  <div class="question">
    Will lunch be provided?
  </div>
  <div class="answer">
    Participants will be provided with meals and accomodation during the course of the camp.
  </div>
</div>
<hr>
<div class="faqs">
  <div class="question">
    Will I get a certificate?
  </div>
  <div class="answer">
    Yes, participants who attend all seven days of the camp will be given a certificate.
  </div>
</div>
<hr>
<div class="faqs">
  <div class="question">
    What else does the registration cost include?
  </div>
  <div class="answer">
    The registration fee include workshop cost, certificate, and a free software kit which includes an FSMK T-shirt, a CD containg GNU/Linux operating system files, stickers, etc.
  </div>
</div>
<hr>
<div class="faqs">
  <div class="question">
    Does registration confirm my seat?
  </div>
  <div class="answer">
    Registration is the first step. We recommend you pay the fees as soon as possible. Once the upper limit is reached, we will close further confirmations of sats.
  </div>
</div>
<hr>
<div class="faqs">
  <div class="question">
    Do I get a refund in case I don't attend the camp?
  </div>
  <div class="answer">
    No, the registration fee is non-refundable, as we plan our resources accordingly.
  </div>
</div>
<hr>
<div class="faqs">
  <div class="question">
    So what happens after I attend the camp? Are there more opportunities?
  </div>
  <div class="answer">
    Yes, the camp is just the beginning. FSMK will, with the cooperation of you and your friends, set up a GNU/Linux Users Group(GLUG) in your college if you don't have one already. From then on, we will help you hold regular events in your college with help of your department/college. FSMK also holds other events, celebrations, and hack-a-thons which you can participate in. Apart from this we hold Sunday schools on a regular basis on Sundays where free workshops/sessions are conducted.
  </div>
</div>
